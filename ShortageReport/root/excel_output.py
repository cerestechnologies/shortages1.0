'''
Created on Apr 25, 2019

@author: abrady
'''

import pandas as pd
import openpyxl
from pathlib import Path
from openpyxl import styles

import datetime

def create_writer(xlpath):
    return pd.ExcelWriter(path=xlpath)

def write_to_excel(writer, df, sheetname = "Sheet1", cols = None):
    df.to_excel(excel_writer = writer, sheet_name = sheetname, index=False, columns=cols)
    
def Export_To_Excel(datadict):
    wb = openpyxl.Workbook()
    ws = wb.active
    ws.title = "Shortage Report"
    greyfill = styles.colors.Color(rgb='D3D3D3')
    cellfill = styles.fills.PatternFill(patternType='solid', fgColor=greyfill)
    jobpocol = 5
    promisestatuscol = 6
    jobpodatecol = 7
    jobpovaluecol = 8
    ws.cell(row=1, column=1, value="Item")
    ws.cell(row=1, column=2, value='Description')
    ws.cell(row=1, column=3, value="On-Hand")
    ws.cell(row=1, column=4, value="Quantity On Order")
    rowiter = 2
    for item, data in datadict.items():
        data.sort_dicts()
        ws.cell(row=rowiter, column=1, value=item)
        ws.cell(row=rowiter, column=2, value=data.desc)
        ws.cell(row=rowiter, column=3, value=data.onhand)
        ws.cell(row=rowiter, column=4, value=data.posum)
        ws.cell(row=rowiter, column = jobpocol, value="Jobs").fill = cellfill
        ws.cell(row=rowiter, column = promisestatuscol, value="Status").fill = cellfill
        ws.cell(row=rowiter, column = jobpodatecol, value="Issue Date").fill = cellfill
        ws.cell(row=rowiter, column=jobpovaluecol, value="Quantity").fill = cellfill
        for job, values in data.jobs.items():
            rowiter += 1
            ws.cell(row=rowiter, column=jobpocol, value=job)
            if isinstance(values[0],datetime.date):
                ws.cell(row=rowiter, column=jobpodatecol, value=values[0].strftime("%m-%d-%Y"))
            ws.cell(row=rowiter, column=jobpovaluecol, value=values[1])
            ws.cell(row=rowiter, column=promisestatuscol, value=values[2])
            
        rowiter += 1
        ws.cell(row=rowiter, column = jobpocol, value="POs").fill = cellfill
        ws.cell(row=rowiter, column = promisestatuscol, value="Promise Date").fill = cellfill
        ws.cell(row=rowiter, column = jobpodatecol, value="Req Date").fill = cellfill
        ws.cell(row=rowiter, column=jobpovaluecol, value="Quantity").fill = cellfill

        for po, values in data.pos.items():
            if not pd.isna(po):
                rowiter += 1
                ws.cell(row=rowiter, column=jobpocol, value=po)
                if isinstance(values[1], datetime.date):
                    ws.cell(row=rowiter, column=promisestatuscol, value=values[1].strftime("%m-%d-%Y"))
                if isinstance(values[0], datetime.date):
                    ws.cell(row=rowiter, column=jobpodatecol, value=values[0].strftime("%m-%d-%Y"))
                ws.cell(row=rowiter, column=jobpovaluecol, value=values[2])
            
        rowiter += 2    
        
    filepath = str(Path.home()) + "\\Desktop\\Shortages\\"
    filename = "Shortage Report " + datetime.date.today().strftime("%m-%d-%Y") + ".xlsx"
    wb.save(filepath + filename)
        