'''
Created on Apr 24, 2019

@author: abrady
'''

def read_system_settings():
    mypath = "C:\\Users\\abrady\\Desktop\\ODBC Connection Settings File"
    myfile = "test_settings.data"
    system_settings_dict = {}
    with open(mypath + "\\" +  myfile) as settings_file:
        for lines in settings_file:
            if not '#' in lines:
                linekey = lines.strip().split("=")[0].strip()
                linevalue = lines.strip().split("=")[1].strip()
                system_settings_dict[linekey] = linevalue
    return system_settings_dict
    
        