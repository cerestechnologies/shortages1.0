'''
Created on Feb 25, 2020

@author: abrady
'''

import pyodbc
import pandas as pd
import sys
from datetime import date
from root import file_access
from root import excel_output
from root import Shortage_Data

def connect():
    connection_dict = file_access.read_system_settings()
    print(connection_dict)
    connection_str = ('dsn=%s;uid=%s;pwd=%s')
    pyodbc.pooling = False
    
    odbc_connection = pyodbc.connect(connection_str %
                                     (connection_dict['DSN'], # wild card arguments
                                     connection_dict['UserID'],
                                     connection_dict['Password']))
    
    test_cursor = odbc_connection.cursor()
    connection_test(test_cursor)
    test_cursor.close()
    return odbc_connection

def connection_test(cursor):
    try:
        cursor.execute('SELECT "order-no" FROM pub."job-hdr"')
        print("Connection Test: Success \n")
    except:
        print("Connection Test: Failure \n")
        
def Load_Dataframes(cursor):
    items = Load_Items(cursor)
    items = Capitalize_Column(items)
    negativeitems = Load_NegativeOnhand(cursor)
    negativeitems = Capitalize_Column(negativeitems)
    negativeitems = Join_DataframesItems(negativeitems, items)
    materiallog = Load_Materiallog(cursor)
    materiallog = Capitalize_Column(materiallog)
    openshippedjobs = Load_JobHeader(cursor)
    transopenshipped = Join_DataframesJobs(openshippedjobs, materiallog)
    negativetrans = Join_DataframesTrans(negativeitems, transopenshipped)    
    openpos = Load_OpenPOs(cursor)
    negativepos = Join_DataframesPOs(negativeitems, openpos)
    datadict = {}
    #datadict = {row[0]:Calculate_Transaction_History(row, datadict) for row in negativetrans[['item-no', 'tran-date', 'tran-time',
    #                                                                                   'on-hand', 'quantity', 'whse',
    #                                                                                    'order-no']].values}
    datadict = populate_transactions(datadict, negativetrans)
    datadict = populate_pos(datadict, negativepos)
    excel_output.Export_To_Excel(datadict)
        
def populate_transactions(datadict, df):
    for row in df[['item-no', 'tran-date', 'tran-time', 'on-hand', 'quantity', 'whse', 'order-no', 'job-status', 'item-desc']].itertuples():
        if row[1] == 'C01-04509':
            print('here')
        datadict[row[1]] = Calculate_Transaction_History(row, datadict)
    return datadict

def populate_pos(datadict, df):
    for row in df[['item-no', 'qty-ordered', 'req-date', 'promise-date', 'po-no']].itertuples():
        datadict[row[1]] = Calculate_POs(row, datadict)
    return datadict

def Calculate_Transaction_History(row, datadict):
        if not row[1] in datadict:
            data = Shortage_Data.ShortageData()
            data.addrow(row)
        else:
            data = datadict[row[1]].addtransactions(row)

        return data   
def Calculate_POs(row, datadict):
    data = datadict[row[1]].addpos(row)
    return data

def Capitalize_Column(df):
    df['item-no'] = df['item-no'].apply(lambda item:str(item).upper())
    return df

def Load_Items(cursor):
    items = pd.read_sql("""
    SELECT im."item-no", im."item-desc"
    FROM pub."item" AS im
    """, cursor)
    return items

def Load_JobHeader(cursor):
    openshippedjobs = pd.read_sql("""
    SELECT jh."order-no", jh."job-status"
    FROM pub."job-hdr" AS jh
    WHERE jh."job-status" like 'O' OR jh."job-status" like 'S'
    """, cursor)
    
    return openshippedjobs

def Load_OpenPOs(cursor):
    OpenPOs = pd.read_sql("""
    SELECT pol."po-no", pol."line-status", pol."item-no", pol."promise-date", pol."req-date",
    pol."qty-ordered", pol."qty-received", pol."unit-measure", pol."line-status"
    FROM pub."po-line" AS pol 
    WHERE pol."line-status" like 'O' 
    AND pol."qty-received" < pol."qty-ordered" 
    """, cursor)                      
    return OpenPOs

def Load_NegativeOnhand(cursor):
    negativeitems = pd.read_sql("""
    SELECT isl."item-no", isl."on-hand", isl."whse"
    FROM pub."item-stkloc" AS isl
    WHERE isl."on-hand"<0
    AND isl."whse" like 'whse'
    """, cursor)
    negativeitems['item-no']=negativeitems['item-no'].astype(str)
    return negativeitems

def Load_Materiallog(cursor):
    materiallog = pd.read_sql("""
    SELECT ml."item-no", ml."tran-date", ml."tran-time", ml."tran-type", ml."tran-type", ml."quantity", ml."order-no", ml."whse"
    FROM pub."matl-log" AS ml
    WHERE ml."tran-type" like 'JI'
    ORDER BY ml."item-no" ASC, ml."tran-date" ASC, ml."tran-time" ASC
    """, cursor)
    materiallog['item-no']=materiallog['item-no'].astype(str)
    return materiallog

def Join_DataframesPOs(lefttable, righttable):
    joinedtable = pd.merge(left = lefttable, right = righttable, on=['item-no'], how='left')
    joinedtable = joinedtable.sort_values(by=['item-no', 'req-date', 'promise-date'], ascending=[True, False, False])
    return joinedtable

def Join_DataframesTrans(lefttable, righttable):
    joinedtable = pd.merge(left=lefttable,right=righttable,on=['item-no', 'whse'],how='left')
    joinedtable = joinedtable.sort_values(by=['item-no', 'tran-date', 'tran-time'], ascending=[True, False, False])
    return joinedtable

def Join_DataframesJobs(lefttable, righttable):
    joinedtable = pd.merge(left=lefttable, right=righttable, on=['order-no'], how='left')
    joinedtable = joinedtable.sort_values(by=['item-no', 'tran-date', 'tran-time'], ascending=[True, False, False]) 
    return joinedtable   

def Join_DataframesItems(lefttable, righttable):
    joinedtable = pd.merge(left=lefttable, right=righttable, on=['item-no'], how='left')
    return joinedtable

def Send_to_Writer(df):
    output_path = "C:\\Users\\abrady\\Desktop\\Shortages" + "\\"  + str(date.today()) + "ShortageTest.xlsx"
    writer = excel_output.create_writer(output_path)
    with writer:
        
        excel_output.write_to_excel(writer, df, "Final")
