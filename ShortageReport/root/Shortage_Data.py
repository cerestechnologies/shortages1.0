'''
Created on Feb 26, 2020

@author: abrady
'''
import pandas
from collections import OrderedDict
class ShortageData(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.jobs = OrderedDict()
        self.pos = OrderedDict()
        self.transtoggle = False
        self.posum = 0.0
        
    def addrow(self, row):
        self.whse = row[6]
        self.onhand = row[4]
        self.transsum = row[5]
        self.desc = row[9]
        self.jobs[row[7]] = (row[2], row[5], row[8])
        
    def addtransactions(self, row):
        if self.transtoggle == False:
            if self.transsum + self.onhand >= 0:
                self.transtoggle = True
            if not row[7] in self.jobs:
                self.jobs[row[7]] = (row[2], row[5], row[8])
            else:
                self.jobs[row[7]] = (row[2], row[5] + self.jobs[row[7]][1], row[8])
                
            self.transsum += row[5]
        return self
    
    def addpos(self, row):
        if not pandas.isna(row[2]):
            self.posum += row[2]
        self.pos[row[5]] = (row[3], row[4], row[2])
        return self
    
    def sort_dicts(self):
        self.jobs = {k:v for k, v in sorted(self.jobs.items(), key=lambda item: item[1], reverse = False)}
        self.pos = {k:v for k, v in sorted(self.pos.items(), key=lambda item: item[1], reverse = False)}